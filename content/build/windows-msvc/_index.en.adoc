---
title: Windows (Visual Studio)
weight: 14
summary: Guide on building KiCad using Microsoft Visual Studio and vcpkg
tags: ["windows"] 
---
:toc:

== Building using Visual Studio (2019)

=== Environment Setup

==== Visual Studio
You must first install https://visualstudio.microsoft.com/vs/[Visual Studio] with the **Desktop development with C++** feature set installed.
Additionally, you'll need to make sure the optional component https://docs.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio?view=msvc-160#installation[C{plus}{plus} CMake tools for Windows] is installed.

==== vcpkg
**If you are new to vcpkg** you must, pick a spot on your system to put it.
Then run these three commands

[source,powershell]
```
git clone https://github.com/microsoft/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
.\vcpkg\vcpkg integrate install
```

which will give you a vcpkg install ready to use with the next steps

=== KiCad Specific Setup

vcpkg defaults to x86-windows even on 64-bit machines,
it is advised for ease of use you set a **USER** or **SYSTEM** environment variable
with the name **VCPKG_DEFAULT_TRIPLET** and value **x64-windows**

KiCad still supports 32-bit builds for now but may not in the future, thus 64-bit is preferred.

==== 1. Install vcpkg packages
The following packages are required for vcpkg

[source,powershell]
```
.\vcpkg install boost
.\vcpkg install cairo
.\vcpkg install curl
.\vcpkg install glew
.\vcpkg install gettext
.\vcpkg install glm
.\vcpkg install icu
.\vcpkg install ngspice
.\vcpkg install opencascade
.\vcpkg install opengl
.\vcpkg install openssl
.\vcpkg install python3
.\vcpkg install wxwidgets
.\vcpkg install zlib
```

If you did not set the **VCPKG_DEFAULT_TRIPLET** environment variable, you will have to append
:x64-windows to end of each packages name, `boost:x64-windows` for example.

==== 2. CMakeSettings.json
Contained in the build root is a `CMakeSettings.json.sample`, copy and rename this file to `CMakeSettings.json`
Edit `CMakeSettings.json` update the VcPkgDir environment variable up top to match the location of your vcpkg clone.

[source,json]
----
{ "VcPkgDir": "D:/vcpkg/" },
----

==== 3. "Open Folder" in Visual Studio
Launch Visual Studio (only after completing the above steps).

When the initial wizard launches, select to **Open a local folder**
This is the correct way to make Visual Studio directly handle *CMake* projects.
